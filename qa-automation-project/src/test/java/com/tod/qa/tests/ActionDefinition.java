//Functions Library of all generic action definitions

package com.tod.qa.tests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.tod.qa.pageObjects.LoginPage;
import com.tod.qa.utilities.Base;
import com.tod.qa.utilities.ConfigFileReader;

public class ActionDefinition extends Base {

	public WebDriver driver;
	public ExtentTest grandchildTest;

	public ActionDefinition(WebDriver driver) {
		this.driver = driver;
	}

	public static Logger log = LogManager.getLogger(ActionDefinition.class.getName());
	ConfigFileReader configFileReader = new ConfigFileReader();

	LoginPage loginPageObject;

	public void navigateToUrl(String action, String url, ExtentTest grandchildTest) throws IOException {
		try {
			this.driver.get(url);
			log.info("Navigated to URL : " + url);
			grandchildTest.log(Status.PASS, MarkupHelper.createLabel("Test Step Passed : Actual Result : " + driver.getCurrentUrl(), ExtentColor.GREEN));
		} catch (Exception e) {
			grandchildTest.log(Status.FAIL, MarkupHelper.createLabel("Test Step Failed" + e, ExtentColor.RED));
			getchildScreenshot(action, driver, grandchildTest);
		}
	}

	public void maximizeWindow(String action, ExtentTest grandchildTest) {
		try {
			this.driver.manage().window().maximize();
			log.info("Window Maximized");
			grandchildTest.log(Status.PASS, MarkupHelper.createLabel("Test Step Passed", ExtentColor.GREEN));
		} catch (Exception e) {
			grandchildTest.log(Status.FAIL, MarkupHelper.createLabel("Test Step Failed" + e, ExtentColor.RED));
			getchildScreenshot(action, driver, grandchildTest);
		}
	}

	public void implicitWait(String action, String data, ExtentTest grandchildTest) {
		try {
			int waitTime = (int) Double.parseDouble(data);
			driver.manage().timeouts().implicitlyWait(waitTime, TimeUnit.SECONDS);
			log.info("Waited for" + waitTime + "Seconds");
			grandchildTest.log(Status.PASS, MarkupHelper.createLabel("Test Step Passed", ExtentColor.GREEN));
		} catch (Exception e) {
			grandchildTest.log(Status.FAIL, MarkupHelper.createLabel("Test Step Failed" + e, ExtentColor.RED));
			getchildScreenshot(action, driver, grandchildTest);
		}
	}

	public void enterUsername(String action, String username, ExtentTest grandchildTest) {
		try {
			loginPageObject = new LoginPage(driver);
			loginPageObject.username().clear();
			loginPageObject.username().sendKeys(username);
			log.info("Username entered : " + username);
			grandchildTest.log(Status.PASS, MarkupHelper.createLabel("Test Step Passed: Actual Result : " + username, ExtentColor.GREEN));
		} catch (NoSuchElementException e) {
			grandchildTest.log(Status.FAIL, MarkupHelper.createLabel("Test Step Failed" + e, ExtentColor.RED));
			getchildScreenshot(action, driver, grandchildTest);
		}
	}

	public void enterPassword(String action, String password, ExtentTest grandchildTest) {
		try {
			loginPageObject = new LoginPage(driver);
			loginPageObject.password().clear();
			loginPageObject.password().sendKeys(password);
			log.info("Password entered");
			grandchildTest.log(Status.PASS, MarkupHelper.createLabel("Test Step Passed: Actual Result : " + password, ExtentColor.GREEN));
		} catch (NoSuchElementException e) {
			grandchildTest.log(Status.FAIL, MarkupHelper.createLabel("Test Step Failed" + e, ExtentColor.RED));
			getchildScreenshot(action, driver, grandchildTest);
		}
	}

	public void clickOnLoginButton(String action, ExtentTest grandchildTest) {
		try {
			loginPageObject = new LoginPage(driver);
			loginPageObject.loginButton().click();
			log.info("Login button clicked");
		} catch (Exception e) {
			grandchildTest.log(Status.FAIL, MarkupHelper.createLabel("Test Step Failed: " + e, ExtentColor.RED));
			getchildScreenshot(action, driver, grandchildTest);
		}
	}
	
	public void validateTitle(String action, ExtentTest grandchildTest ,String expectedResult){
		try {
			if (driver.getTitle().equals(expectedResult)) {
				log.info("Title Match");
				grandchildTest.log(Status.PASS, MarkupHelper.createLabel("Test Step Passed: Actual Result : " + driver.getTitle(), ExtentColor.GREEN));
			} else {
				log.info("Title Mismatch");
				throw new Exception("Title Mismatch");
			}
		} catch (Exception e) {
			grandchildTest.log(Status.FAIL, MarkupHelper.createLabel("Test Step Failed: " + e, ExtentColor.RED));
			getchildScreenshot(action, driver, grandchildTest);
		}
	}
	
	
}
