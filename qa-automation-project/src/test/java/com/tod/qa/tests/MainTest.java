package com.tod.qa.tests;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.tod.qa.utilities.Base;
import com.tod.qa.utilities.ExtentReporterNg;

public class MainTest extends Base {

	public static Logger log = LogManager.getLogger(ActionDefinition.class.getName());
	ExtentTest parentTest;
	ExtentTest childTest;
	ExtentReports extent = ExtentReporterNg.getReportObject();

	public WebDriver driver;
	String methodName;

	@BeforeSuite
	public void initialize() throws IOException {
		driver = initializeDriver();
		log.info("Driver is initialized");
	}

	@Test
	public void Runner() throws IOException {
		ExcelReader XLReader = new ExcelReader(driver);
		XLReader.runTest();
		log.info("Inside readXLDataFile");
	}

	@AfterSuite
	public void tearDown() {
		driver.close();
		log.info("Browser closed");
	}

}
