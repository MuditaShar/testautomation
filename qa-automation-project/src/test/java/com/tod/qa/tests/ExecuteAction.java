package com.tod.qa.tests;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.tod.qa.utilities.ExtentReporterNg;


public class ExecuteAction{

	public WebDriver driver;
	public static Logger log = LogManager.getLogger(ActionDefinition.class.getName());
	public ExtentTest grandchildTest;
	ExtentReports extent = ExtentReporterNg.getReportObject();
	ExcelReader XLReader = new ExcelReader(driver);
	
	public ExecuteAction(WebDriver driver) {

		this.driver = driver;
	}

	public void execute(String action, String data, String expectedResult, ExtentTest childTest) throws IOException {

		ActionDefinition loginTest = new ActionDefinition(driver);
		switch (action) {
		case "navigateToUrl":
			grandchildTest = childTest.createNode(action);
			if (data == null) {
				log.info("No Test Data in navigateToUrl");
				grandchildTest.log(Status.FAIL,MarkupHelper.createLabel("Test Step Failed - No Test Data", ExtentColor.RED));
			} else {
				loginTest.navigateToUrl(action,data,grandchildTest);
			}
			break;
		case "maximizeWindow":
			grandchildTest = childTest.createNode(action);
			loginTest.maximizeWindow(action,grandchildTest);
			break;
		case "waitInSeconds":
			grandchildTest = childTest.createNode(action);
			if (data == null) {
				log.info("No Test Data in waitInSeconds");
				grandchildTest.log(Status.FAIL, MarkupHelper.createLabel("Test Step Failed - No Test Data", ExtentColor.RED));	
			} else {
				loginTest.implicitWait(action,data,grandchildTest);
			}
			break;
		case "enterUsername":
			grandchildTest = childTest.createNode(action);
			if (data == null) {
				log.info("No Test Data in enterUsername");
				grandchildTest.log(Status.FAIL, MarkupHelper.createLabel("Test Step Failed - No Test Data", ExtentColor.RED));
			} else {
					loginTest.enterUsername(action,data,grandchildTest);
				} 
			break;
		case "enterPassword":
			grandchildTest = childTest.createNode(action);
			if (data == null) {
				log.info("No Test Data in enterPassword");
				grandchildTest.log(Status.FAIL, MarkupHelper.createLabel("Test Step Failed - No Test Data", ExtentColor.RED));
			} else {
					loginTest.enterPassword(action,data,grandchildTest);
			}
			break;
		case "clickOnLoginButton":
			grandchildTest = childTest.createNode(action);
			loginTest.clickOnLoginButton(action ,grandchildTest);
			break;
		case "validateTitle":
			grandchildTest = childTest.createNode(action);
			if (expectedResult == null) {
				log.info("No expected title given");
				grandchildTest.log(Status.FAIL, MarkupHelper.createLabel("Test Step Failed - No expected result given", ExtentColor.RED));
			} else {
			loginTest.validateTitle(action,grandchildTest,expectedResult);
			}
			break;
		case "validateErrorMessage":
			grandchildTest = childTest.createNode(action);
			if (expectedResult == null) {
				log.info("No expected title given");
				grandchildTest.log(Status.FAIL, MarkupHelper.createLabel("Test Step Failed - No expected result given", ExtentColor.RED));
			} else {
			loginTest.validateTitle(action,grandchildTest,expectedResult);
			}
			break;
		default:
			grandchildTest = childTest.createNode(action);
			grandchildTest.log(Status.FAIL, MarkupHelper.createLabel("Test Step Failed - No Such Action Defined", ExtentColor.RED));
			break;
		}
	}

}
