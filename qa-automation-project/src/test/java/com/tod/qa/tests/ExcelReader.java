package com.tod.qa.tests;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.tod.qa.utilities.ConfigFileReader;
import com.tod.qa.utilities.ExtentReporterNg;
import com.tod.qa.utilities.Listeners;

public class ExcelReader {

	public WebDriver driver;
	public String workbookName;
	public ExtentTest childTest;
	FileInputStream file;
	ConfigFileReader configFileReader = new ConfigFileReader();
	ExtentReports extent = ExtentReporterNg.getReportObject();
	Listeners listener = new Listeners();


	public ExcelReader(WebDriver driver) {
		this.driver = driver;
	}

	public void runTest() throws IOException {

		file = new FileInputStream(configFileReader.getValueForKey("workbookPath"));
		@SuppressWarnings("resource")
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		int cellIndex = 0;
		List<String> executableTests = new ArrayList<>();
		XSSFSheet sheet = workbook.getSheet("Runner");
		for (int r = sheet.getFirstRowNum(); r <= sheet.getLastRowNum(); r++) {
			Row row = sheet.getRow(r);
			for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
				Cell cell = row.getCell(c);
				if (cell.getStringCellValue().equalsIgnoreCase("Test Case ID")) {
					cellIndex = c;
					break;
				}
				if (row.getRowNum() > 0 && cell != null && cell.getColumnIndex() == cellIndex) {
					executableTests.add(cell.getStringCellValue());
					break;
				}
			}
		}
		fetchTest(executableTests, workbook);
	}

	@Test
	public void fetchTest(List<String> testIds, Workbook workbook) throws IOException {

		Sheet sheet = workbook.getSheet("TestCases");
		int testCaseNameIndex = 0;
		int actionIndex = 0;
		int testDataIndex = 0;
		int expectedResultIndex = 0;
		ExecuteAction executeAction = new ExecuteAction(driver);
		Set<String> nodesList = new LinkedHashSet<>();
		for (int r = sheet.getFirstRowNum(); r <= sheet.getLastRowNum(); r++) {
			Row row = sheet.getRow(r);
			if (row.getRowNum() == 0) {
				for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
					Cell cell = row.getCell(c);
					if (cell.getStringCellValue().equalsIgnoreCase("Test Case Name")) {
						testCaseNameIndex = c;
					}
					if (cell.getStringCellValue().equalsIgnoreCase("Action")) {
						actionIndex = c;
					}
					if (cell.getStringCellValue().equalsIgnoreCase("Test Data")) {
						testDataIndex = c;
					}
					if (cell.getStringCellValue().equalsIgnoreCase("Expected Result")) {
						expectedResultIndex = c;
					}
				}
			} else {
				Cell cell = row.getCell(testCaseNameIndex);
				if (cell != null && testIds.contains(cell.getStringCellValue())) {
					if(!nodesList.contains(row.getCell(testCaseNameIndex).getStringCellValue())) {
						childTest = listener.parentTest.createNode(row.getCell(testCaseNameIndex).getStringCellValue());
						nodesList.add(row.getCell(testCaseNameIndex).getStringCellValue());
					} 
					Cell expResultCell = row.getCell(expectedResultIndex);
					Cell testDataCell = row.getCell(testDataIndex);
					if (testDataCell != null && row.getCell(testDataIndex).getCellType() == CellType.STRING) {
							if (expResultCell != null && expResultCell.getCellType() == CellType.STRING) {
								executeAction.execute(row.getCell(actionIndex).getStringCellValue(),
										row.getCell(testDataIndex).getStringCellValue(),
										row.getCell(expectedResultIndex).getStringCellValue(),childTest);
							} else if (expResultCell != null && expResultCell.getCellType() == CellType.NUMERIC) {
								executeAction.execute(row.getCell(actionIndex).getStringCellValue(),
										row.getCell(testDataIndex).getStringCellValue(),
										Double.toString(row.getCell(expectedResultIndex).getNumericCellValue()),childTest);
							} else if (expResultCell == null || expResultCell.getCellType() == CellType.BLANK) {
								executeAction.execute(row.getCell(actionIndex).getStringCellValue(),
										row.getCell(testDataIndex).getStringCellValue(), null,childTest);
							}
					} else if (testDataCell != null && row.getCell(testDataIndex).getCellType() == CellType.NUMERIC) {
							if (expResultCell != null && expResultCell.getCellType() == CellType.STRING) {
								executeAction.execute(row.getCell(actionIndex).getStringCellValue(),
										Double.toString(row.getCell(testDataIndex).getNumericCellValue()),
										row.getCell(expectedResultIndex).getStringCellValue(),childTest);
							} else if (expResultCell != null && expResultCell.getCellType() == CellType.NUMERIC) {
								executeAction.execute(row.getCell(actionIndex).getStringCellValue(),
										Double.toString(row.getCell(testDataIndex).getNumericCellValue()),
										Double.toString(row.getCell(expectedResultIndex).getNumericCellValue()),childTest);
							} else if (expResultCell == null || expResultCell.getCellType() == CellType.BLANK) {
								executeAction.execute(row.getCell(actionIndex).getStringCellValue(),
										Double.toString(row.getCell(testDataIndex).getNumericCellValue()), null,childTest);
							}
					} else if (testDataCell == null || row.getCell(testDataIndex).getCellType() == CellType.BLANK) {
							if (expResultCell != null && expResultCell.getCellType() == CellType.STRING) {
								executeAction.execute(row.getCell(actionIndex).getStringCellValue(), null,
										row.getCell(expectedResultIndex).getStringCellValue(),childTest);
							} else if (expResultCell != null && expResultCell.getCellType() == CellType.NUMERIC) {
								executeAction.execute(row.getCell(actionIndex).getStringCellValue(), null,
										Double.toString(row.getCell(expectedResultIndex).getNumericCellValue()),childTest);
							} else if (expResultCell != null || expResultCell.getCellType() == CellType.BLANK) {
								executeAction.execute(row.getCell(actionIndex).getStringCellValue(), null, null,childTest);
							}
					}
				}
			}
		}
	}
}
