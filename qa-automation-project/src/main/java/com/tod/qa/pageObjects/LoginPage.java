package com.tod.qa.pageObjects;

import org.dom4j.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.tod.qa.utilities.ReadXMLData;

public class LoginPage {

	WebDriver driver;
	public Document document;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	ReadXMLData readXML = new ReadXMLData("objectRepository.xml");
	
	By username = By.xpath(readXML.getData("XPath.usernamePath"));
	By password = By.xpath(readXML.getData("XPath.passwordPath"));
	By loginButton = By.xpath(readXML.getData("XPath.loginButtonPath"));
	By errorMessage = By.xpath(readXML.getData("XPath.errorMessagePath"));

	public WebElement username() {
		return driver.findElement(username);
	}
	
	public WebElement password() {
		return driver.findElement(password);
	}
	
	public WebElement loginButton() {
		return driver.findElement(loginButton);
	}
	
	public WebElement errorMessage() {
		return driver.findElement(errorMessage);
	}
	
}
