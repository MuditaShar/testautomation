package com.tod.qa.utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {

	private Properties properties;
	private final String propertyFilePath = "configuration//configurationFile.properties";

	public ConfigFileReader() {

		BufferedReader reader;

		try {
			reader = new BufferedReader(new FileReader(propertyFilePath));
			properties = new Properties();
			try {
				properties.load(reader);
				reader.close();
			} catch (IOException e) {

				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {

			e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		}
	}
	
	public String getValueForKey(String Key) {
		String value = properties.getProperty(Key);
		if (value != null)
			return value;
		else
			throw new RuntimeException(Key + "not specified in the Configuration.properties file.");
	}

	public String dataRequired(String actionName) {
		String dataRequired = properties.getProperty(actionName);
		if (dataRequired != null)
			return dataRequired;
		else
			throw new RuntimeException("Data check not specified in the Configuration.properties file for" + actionName);
	}

}