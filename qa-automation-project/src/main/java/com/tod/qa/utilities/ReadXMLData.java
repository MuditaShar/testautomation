package com.tod.qa.utilities;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

public class ReadXMLData {
	
	public Document document;
	public String fileName;
	 
	public ReadXMLData(String fileName) {
	this.fileName = fileName;
	}

	public String getData(String locator) {
		
	File inputFile = new File(System.getProperty("user.dir") + File.separator+"configuration"+File.separator+fileName);
    SAXReader saxReader = new SAXReader();
    try {
    document = saxReader.read(inputFile);
    } catch (DocumentException e) {
    	e.printStackTrace();
    }
    String data = document.selectSingleNode("//" + locator.replace('.', '/')).getText(); 
    return data;
    
	}
	
}
