package com.tod.qa.utilities;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Base  {

	public WebDriver driver;
	ConfigFileReader configFileReader = new ConfigFileReader();

	public WebDriver initializeDriver() throws IOException {

		String browserName = configFileReader.getValueForKey("browser");
		String Chrome_Profile_Path = configFileReader.getValueForKey("Chrome_Profile_Path");
		if (browserName.equals("chrome")) {
			WebDriverManager.chromedriver().setup();
			ChromeOptions Chrome_Profile = new ChromeOptions();
			/* Disabling the Chrome browser extensions */
			Chrome_Profile.addArguments("chrome.switches","--disable-extensions"); 
			/* Adding Chrome profile by .addArguments to objChrome_Profile  */
			Chrome_Profile.addArguments("user-data-dir=" + Chrome_Profile_Path);
			driver = new ChromeDriver(Chrome_Profile);
			driver.manage().deleteAllCookies();
		} else if (browserName.equals("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		} else if (browserName.equals("IE")) {
			WebDriverManager.iedriver().setup();
			driver = new InternetExplorerDriver();
		}
		return driver;
	}

	
	public String getScreenShot(String testMethodName, WebDriver driver) throws IOException {

		TakesScreenshot shot = (TakesScreenshot) driver;
		File source = shot.getScreenshotAs(OutputType.FILE);
		String destinationFile = System.getProperty("user.dir") + "\\reports\\" + testMethodName + ".png";
		FileUtils.copyFile(source, new File(destinationFile));
		return destinationFile;
	}
	
	public void getchildScreenshot(String action, WebDriver driver, ExtentTest childTest ) {
		try {
			childTest.addScreenCaptureFromPath(getScreenShot(action,driver),action);
		} catch (IOException e1) {
			childTest.log(Status.FAIL, MarkupHelper.createLabel("Test Step Failed: "+ e1, ExtentColor.RED));
		}
	}
	
}
