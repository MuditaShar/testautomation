package com.tod.qa.utilities;

import java.io.File;
import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ExtentReporterNg {

	public static ExtentReports extent;
	public static ExtentSparkReporter reporter;
	
	public static ExtentReports getReportObject() {
		
		ConfigFileReader configFileReader= new ConfigFileReader();
		String testerName = configFileReader.getValueForKey("tester");
		
		String path =System.getProperty("user.dir")+"\\reports\\index.html";
		reporter = new ExtentSparkReporter(path);
		extent = new ExtentReports();
		extent.attachReporter(reporter);
		extent.setSystemInfo("Tester", testerName);
		extent.setSystemInfo("OS : ", System.getProperty("os.name"));
		extent.setSystemInfo("OS Architecture : ", System.getProperty("os.arch"));
		extent.setSystemInfo("Java Version : ", System.getProperty("java.version"));

		try {
		File configFile = new File(System.getProperty("user.dir")+"\\configuration\\extent-config.xml");
		reporter.loadXMLConfig(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return extent;
	
	}
}
